# uTerror Amp

Guitar amplifier. Made with KiCAD.

This is my attempt at making available the schematic from the solid-state/valve hybrid amp from the colourful british brand we all know and love.
This was made with a lot of patience, a multimeter in continuity test mode and a semi-powerful led flashlight to see the traces in the board.

I can't guarentee this circuit is correct and/or if it works. 
Any pcb designs in this repo are tests, use them at your own risk, if you're not planning on drawing the board yourself. They don't pretend to be identical to the original one.

I'm not a scientist or an electronics engineer, I'm merely a curious student and a (not very)competent guitar player who values free information.
If your know about anything about analog electronics, though, please feel free to add a file explaining the sections of the circuit or add comments in the schematic file and I'll be happy to merge them.

If you're just looking to build your amp or to repair a broken one, I hope this helps you and have fun!

About the license, I added what I found about Open Source Hardware. I'm not even sure if that classifies as a license, but regardless, please follow those principles. Thank you.